from db_connection.mysql_local import db
from tool.check_valid_word import check_valid_word

class Standard0(db.Model):
    __tablename__ = 'standard0'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value
    def check_acronym_word(self):
        text = self.value
        ls_word = text.split(' ')
        for word in ls_word:
            for acronym in acronym_predicts:
                # print(acronym.pr)
                if acronym.predict_value == word:
                    # print(word)
                    return True
        return False

class Standard1(db.Model):
    __tablename__ = 'standard1'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class Standard2(db.Model):
    __tablename__ = 'standard2'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class Standard3(db.Model):
    __tablename__ = 'standard3'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class NotAccent0(db.Model):
    __tablename__ = 'not_accent0'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class NotAccent1(db.Model):
    __tablename__ = 'not_accent1'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class NotAccent2(db.Model):
    __tablename__ = 'not_accent2'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class NotAccent3(db.Model):
    __tablename__ = 'not_accent3'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class NotAccentVal(db.Model):
    __tablename__ = 'not_accent_val'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value

class StandardVal(db.Model):
    __tablename__ = 'standard_val'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

    def __init__(self, value=None):
        self.value = value


class Vowel0(db.Model):
    __tablename__ = 'vowel0'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(50))
    raw_telex1 = db.Column(db.String(50)) # chua truoc, dau sau
    raw_telex1_2 = db.Column(db.String(50))
    raw_telex2 = db.Column(db.String(50)) # chu sau, dau sau
    raw_telex2_2 = db.Column(db.String(50))
    raw_telex3 = db.Column(db.String(50)) # chu truoc, dau truoc
    raw_telex3_2 = db.Column(db.String(50))
    not_tone = db.Column(db.String(50))

class Vowel3(db.Model):
    __tablename__ = 'vowel3'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(50))
    back = db.Column(db.Boolean)
    back_conso = db.Column(db.Boolean)

class Vowel2(db.Model):
    __tablename__ = 'vowel2'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(50))
    back = db.Column(db.Boolean)
    back_conso = db.Column(db.Boolean)

class Vowel(db.Model):
    __tablename__ = 'vowel'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(50))
    raw_telex1 = db.Column(db.String(50)) # chua truoc, dau sau
    raw_telex1_2 = db.Column(db.String(50))
    raw_telex2 = db.Column(db.String(50)) # chu sau, dau sau
    raw_telex2_2 = db.Column(db.String(50))
    raw_telex3 = db.Column(db.String(50)) # chu truoc, dau truoc
    raw_telex3_2 = db.Column(db.String(50))

    def __init__(self, value=None):
        self.value = value

class RawTelex0(db.Model):
    __tablename__ = 'raw_telex0'
    id = db.Column(db.Integer, primary_key=True)
    standard_id = db.Column(db.Integer)
    raw_telex = db.Column(db.Text) # mix
    raw_telex1 = db.Column(db.Text) # chua truoc, dau sau
    raw_telex2 = db.Column(db.Text) # chu sau, dau sau
    raw_telex3 = db.Column(db.Text) # chu truoc, dau truoc

    def __init__(self, standard_id=None, raw_telex=None, raw_telex1=None, raw_telex2=None, raw_telex3=None):
        self.standard_id = standard_id
        self.raw_telex = raw_telex
        self.raw_telex1 = raw_telex1
        self.raw_telex2 = raw_telex2
        self.raw_telex3 = raw_telex3

class RawTelex1(db.Model):
    __tablename__ = 'raw_telex1'
    id = db.Column(db.Integer, primary_key=True)
    standard_id = db.Column(db.Integer)
    raw_telex = db.Column(db.Text) # mix
    raw_telex1 = db.Column(db.Text) # chua truoc, dau sau
    raw_telex2 = db.Column(db.Text) # chu sau, dau sau
    raw_telex3 = db.Column(db.Text) # chu truoc, dau truoc

    def __init__(self, standard_id=None, raw_telex=None, raw_telex1=None, raw_telex2=None, raw_telex3=None):
        self.standard_id = standard_id
        self.raw_telex = raw_telex
        self.raw_telex1 = raw_telex1
        self.raw_telex2 = raw_telex2
        self.raw_telex3 = raw_telex3

class RawTelex2(db.Model):
    __tablename__ = 'raw_telex2'
    id = db.Column(db.Integer, primary_key=True)
    standard_id = db.Column(db.Integer)
    raw_telex = db.Column(db.Text) # mix
    raw_telex1 = db.Column(db.Text) # chua truoc, dau sau
    raw_telex2 = db.Column(db.Text) # chu sau, dau sau
    raw_telex3 = db.Column(db.Text) # chu truoc, dau truoc

    def __init__(self, standard_id=None, raw_telex=None, raw_telex1=None, raw_telex2=None, raw_telex3=None):
        self.standard_id = standard_id
        self.raw_telex = raw_telex
        self.raw_telex1 = raw_telex1
        self.raw_telex2 = raw_telex2
        self.raw_telex3 = raw_telex3

class RawTelex3(db.Model):
    __tablename__ = 'raw_telex3'
    id = db.Column(db.Integer, primary_key=True)
    standard_id = db.Column(db.Integer)
    raw_telex = db.Column(db.Text) # mix
    raw_telex1 = db.Column(db.Text) # chua truoc, dau sau
    raw_telex2 = db.Column(db.Text) # chu sau, dau sau
    raw_telex3 = db.Column(db.Text) # chu truoc, dau truoc

    def __init__(self, standard_id=None, raw_telex=None, raw_telex1=None, raw_telex2=None, raw_telex3=None):
        self.standard_id = standard_id
        self.raw_telex = raw_telex
        self.raw_telex1 = raw_telex1
        self.raw_telex2 = raw_telex2
        self.raw_telex3 = raw_telex3

class RawTelexVal(db.Model):
    __tablename__ = 'raw_telex_val'
    id = db.Column(db.Integer, primary_key=True)
    standard_id = db.Column(db.Integer)
    raw_telex = db.Column(db.Text) # mix
    raw_telex1 = db.Column(db.Text) # chua truoc, dau sau
    raw_telex2 = db.Column(db.Text) # chu sau, dau sau
    raw_telex3 = db.Column(db.Text) # chu truoc, dau truoc

    def __init__(self, standard_id=None, raw_telex=None, raw_telex1=None, raw_telex2=None, raw_telex3=None):
        self.standard_id = standard_id
        self.raw_telex = raw_telex
        self.raw_telex1 = raw_telex1
        self.raw_telex2 = raw_telex2
        self.raw_telex3 = raw_telex3


class Acronym(db.Model):
    __tablename__ = 'acronym'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(45))
    nb_appearance = db.Column(db.Integer)
    predict_value = db.Column(db.String(45))


class AcronymPredict(db.Model):
    __tablename__ = 'acronym_predict'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(45))
    nb_appearance = db.Column(db.Integer)
    predict_value = db.Column(db.String(45))

acronym_predicts = AcronymPredict.query.all()

class Stand0sub(db.Model):
    __tablename__ = 'stand0sub'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

class Neu(db.Model):
    __tablename__ = 'neu'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

class Stand0sub_acronym(db.Model):
    __tablename__ = 'stand0sub_acronym'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

class Stand0subNotAccent(db.Model):
    __tablename__ = 'stand0sub_not_accent'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

class Stand0subAcronymNotAccent(db.Model):
    __tablename__ = 'stand0sub_acronym_not_accent'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)


class Standval_acronym(db.Model):
    __tablename__ = 'standval_acronym'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

class StandvalAcronymNotAccent(db.Model):
    __tablename__ = 'standval_acronym_not_accent'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)

class NeuSrc(db.Model):
    __tablename__ = 'neu_src'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Text)
    file_name = db.Column(db.String(100))

    def detect_invalid_word(self):
        text = self.value.lower()
        ls_words = text.split(' ')
        # print(self.value)
        ls_acronym = []
        for word in ls_words:
            if check_valid_word(word) == False and word != '':
                ls_acronym.append(word)
        return ls_acronym

    def check_acronym_word(self):
        text = self.value
        ls_word = text.split(' ')
        for word in ls_word:
            for acronym in acronym_predicts:
                # print(acronym.pr)
                if acronym.predict_value == word:
                    # print(word)
                    return True
        return False

    def clean_specail_character(self):
        ls_specail_character = [':', '!', '@', '-', '+']
        comment = self.value
        print(self.id)
        for i in ls_specail_character:
            comment = comment.replace(i, '')
        ls = comment.split(' ')
        for value in ls:
            value = value.lower()
            count = 0
            vowel0s = Vowel0.query.all()
            for i in vowel0s:
                if i.value in value:
                    count = count + 1
            consonants = Consonant.query.all()
            for i in consonants:
                if i.value in value:
                    count = count + 1
            if count == 0:
                print(value)
                comment = comment.replace(value, '')
        self.value = comment
        db.session.commit()

    def cl_check_character(self):
        # if have common character => true
        # just special character => false
        value = self.value
        value = value.lower()
        count = 0
        vowel0s = Vowel0.query.all()
        for i in vowel0s:
            if i.value in value:
                count = count + 1
        consonants  =Consonant.query.all()
        for i in consonants:
            if i.value in value:
                count = count + 1
        if count == 0:
            return False
        return True

class Consonant(db.Model):
    __tablename__ = 'consonant'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(45))
    at_end = db.Column(db.String(45))



db.create_all()

if __name__ == '__main__':
    neu = NeuSrc(value='tao chi la hoc sinh')
    # neu.clean_specail_character()
    print(neu.check_acronym_word())
    pass
