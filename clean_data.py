from model.models import *
import os
import glob
import errno
from time import sleep
from subprocess import call
from db_connection.mysql_local import db
from datetime import datetime

dir = os.getcwd()

min = 0
while min < 640000:
    start = datetime.now()
    neu_src = NeuSrc.query.filter(NeuSrc.id<=min+1000).filter(NeuSrc.id>min).all()
    print(len(neu_src))
    for i in neu_src:
        neu = Neu.query.filter(Neu.value==i.value).first()
        if not neu and len(i.value)<300:
            neu = Neu(value=i.value)
            print(i.id, i.value)
            db.session.add(neu)
            db.session.commit()
    min = min +1000
    print("Duration:", datetime.now()-start)
