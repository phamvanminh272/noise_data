from model.models import NeuSrc
from datetime import datetime
from audit_service.model.model import AuditStep, AuditJob

def detect_acronym():
    job = AuditJob(name='Get acronym of NeuSrc to acronym2.txt')
    job.start()
    min = 0
    while min < 10000:
        step = AuditStep(job=job, name='Get acronym of 10000 rows.')
        step.start()
        neu_srcs = NeuSrc.query.filter(NeuSrc.id<=min+10000).filter(NeuSrc.id>min).all()
        print(len(neu_srcs))
        for i in neu_srcs:
            ls_acronym = i.detect_invalid_word()
            print(i.id, ls_acronym)
            for word in ls_acronym:
                with open('acronym3.txt', 'a', newline='') as f:
                    f.writelines(word + '\n')
        min = min +10000
        step.end()
        print('Duration:',step.duration)
    job.end()
    print(job.duration)