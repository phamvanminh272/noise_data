from model.models import Standard0, acronym_predicts, Stand0sub, StandardVal
from db_connection.mysql_local import db
from audit_service.model.model import AuditStep, AuditJob
from tool.check_valid_word import cut_word_component, clean_tone

def convert_text_to_acronym():
    job = AuditJob(name = 'Convert stand0sub to acronym')
    job.start()
    # stand0subs = Stand0sub.query.all()
    # stand0subs = Stand0sub.query.filter(Stand0sub.id<6).all()
    stand0subs = StandardVal.query.all()
    for stand in stand0subs:
        text = stand.value
        text = text.replace(u'\n', '')
        ls = text.split(' ')
        text = ''
        for word in ls:
            for acronym in acronym_predicts:
                # print(acronym.predict_value)
                if acronym.predict_value == word:
                    # print(acronym.predict_value)
                    word = acronym.value
            item = cut_word_component(clean_tone(word))
            if item['front_conso'] != '' and item['front_conso'] != 'ng' and item['back_conso'] == 'ng':
                word = word.replace('ng', 'g')
            text = text + word + ' '
        text = text[:-1] + '\n'
        sql = ''
        if '"' in text:
            sql = "insert into standval_acronym(`value`) values('" + text + "')"
        else:
            sql = 'insert into standval_acronym(`value`) values("' + text + '")'
        # print(sql)
        db.engine.execute(sql)
        print(stand.id, text)
    job.end()
    print('Duration:', job.duration)


# convert_text_to_acronym()
def acronym_ng():
    job = AuditJob(name='Convert stand0sub to acronym with ng -> g')
    job.start()
    # stand0subs = Stand0sub.query.all()
    stand0subs = Stand0sub.query.filter(Stand0sub.id <6).all()
    for stand in stand0subs:
        text = stand.value
        text = text.replace(u'\n', '')
        ls = text.split(' ')
        text = ''
        for word in ls:
            item = cut_word_component(clean_tone(word))
            if item['front_conso'] != '' and item['front_conso'] != 'ng' and item['back_conso'] == 'ng':
                word = word.replace('ng', 'g')
            text = text + word + ' '
        text = text[:-1] + '\n'
        sql = ''
        if '"' in text:
            sql = "insert into stand0sub_acronym(`value`) values('" + text + "')"
        else:
            sql = 'insert into stand0sub_acronym(`value`) values("' + text + '")'
        # print(sql)
        # db.engine.execute(sql)
        print(stand.id, text)
    job.end()

# acronym_ng()

