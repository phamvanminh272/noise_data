
from datetime import datetime
from queue import Queue
from threading import Thread
import os


def is_root_path_project(path, project_name):
    # print("====", path)
    ls=path.split("/")
    # print("000", ls)
    nb=len(ls)
    # print("+++",ls[nb-1])
    # print("---",ls[-1])
    if ls[-1]==project_name:
        return True
    return False

def get_root_project_path(project_name):
    path = os.path.dirname(os.path.realpath(""))
    path = path.replace("\\", '/')
    # print(path)
    max_loop_nb = 20
    while not is_root_path_project(path, project_name) and max_loop_nb > 0:
        path = os.path.dirname(path)
        # print(path)
        max_loop_nb = max_loop_nb - 1
    # print(os.path.dirname(os.path.dirname(os.path.dirname(inspect.getfile(inspect.currentframe())))) + "/INPUT/company")
    return path






def thread(q, crawling_func):
    while not q.empty():
        x = q.get()
        i = x[1]
        crawling_func(i)
        q.task_done()
    return True


def engine_crawling_thread(ls, unit, num_threads=20):
    start_time = datetime.now()
    q = Queue(maxsize=0)
    # results = [{} for x in ls_comp]
    for i in range(len(ls)):
        # need the index and the url in each queue item.
        q.put((i, ls[i]))

    for i in range(num_threads):
        worker = Thread(target=thread, args=(q, unit))
        worker.setDaemon(True)  # setting threads as "daemon" allows main program to
        worker.start()
    # now we wait until the queue has been processed
    q.join()

    end_time = datetime.now()
    duration = end_time - start_time
    print("TOTAL DURATION:", duration)





