# what is invalid word
# 1: do not have vowel or vowel is not exist
# 2: back-consonant is not exist or not valid (some consonants cannot stand at the back)
# 3: Some vowels must have back-consonant
# 4: some vowels do not have back-consonant

# how to use check valid word: check_valid_word(word). It will return True or False.
ls_vowel_tone = ['á', 'ắ', 'ấ', 'é', 'ế', 'í', 'ó', 'ố', 'ớ', 'ú', 'ứ', 'ý', 'à', 'ằ', 'ầ', 'è', 'ề', 'ì', 'ò', 'ồ',
                 'ờ', 'ù', 'ừ', 'ỳ', 'ả', 'ẳ', 'ẩ', 'ẻ',
                 'ể', 'ỉ', 'ỏ', 'ổ', 'ở', 'ủ', 'ử', 'ỷ', 'ạ', 'ặ', 'ậ', 'ẹ', 'ệ', 'ị', 'ọ', 'ộ', 'ợ', 'ụ', 'ự', 'ỵ',
                 'ã', 'ẵ', 'ẫ', 'ẽ', 'ễ', 'ĩ', 'õ', 'ỗ', 'ỡ', 'ũ', 'ữ', 'ỹ', 'ờ', 'à']
ls_vowel_not_tone = ['a', 'ă', 'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ', 'u', 'ư', 'y', 'a', 'ă', 'â', 'e', 'ê', 'i', 'o',
                     'ô', 'ơ', 'u', 'ư', 'y', 'a', 'ă', 'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ', 'u', 'ư', 'y', 'a', 'ă',
                     'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ', 'u', 'ư', 'y', 'a', 'ă', 'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ',
                     'u', 'ư', 'y', 'ơ', 'a']
ls_vowel1 = ['a', 'ă', 'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ', 'u', 'ư', 'y']
ls_vowel1_not_accent = ['a', 'a', 'a', 'e', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'y']
ls_vowel2 = ['ai', 'ao', 'au', 'âu', 'ay', 'ây', 'eo', 'êu', 'ia', 'iê', 'yê', 'iu', 'oa', 'oă', 'oe', 'oi', 'ôi',
             'ơi', 'oo', 'ôô', 'ua', 'uă', 'uâ', 'ưa', 'uê', 'ui', 'ưi', 'uo', 'uô', 'uơ', 'ươ', 'ưu', 'uy']
ls_vowel3 = ['iêu', 'yêu', 'oai', 'oao', 'oay', 'oeo', 'uao', 'uây', 'uôi', 'ươi', 'ươu', 'uya', 'uyê', 'uyu']
ls_vowel = ['iêu', 'yêu', 'oai', 'oao', 'oay', 'oeo', 'uao', 'uây', 'uôi', 'ươi', 'ươu', 'uya', 'uyê', 'uyu',
            'ai', 'ao', 'au', 'âu', 'ay', 'ây', 'eo', 'êu', 'ia', 'iê', 'yê', 'iu', 'oa', 'oă', 'oe', 'oi', 'ôi',
            'ơi', 'oo', 'ôô', 'ua', 'uă', 'uâ', 'ưa', 'uê', 'ui', 'ưi', 'uo', 'uô', 'uơ', 'ươ', 'ưu', 'uy',
            'a', 'ă', 'â', 'e', 'ê', 'i', 'o', 'ô', 'ơ', 'u', 'ư', 'y']
ls_conso_not_at_end = ['gh', 'kh', 'ngh', 'ph', 'th', 'tr', 'gi', 'qu', 'b', 'd', 'đ', 'g', 'h', 'k', 'l', 'q', 'r',
                       's', 'v', 'x']
ls_conso_at_end = ['ch', 'ng', 'nh', 'c', 'm', 'n', 'p', 't']
ls_conso = ['ch', 'gh', 'kh', 'ngh', 'ng', 'nh', 'ph', 'th', 'tr', 'gi', 'qu', 'b', 'c', 'd', 'đ', 'g', 'h', 'k', 'l',
            'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'x']
ls_special_character = [',', '.', '?', ':', "'", '"', '`', "!", ")", "(", '\n', """
"""]

# con = Vowel0.query.all()
# for i in con:
#     ls_vowel_not_tone.append(i.not_tone)
# print(ls_vowel_not_tone)

def clean_tone(word):
    """
    Convert word to untone word ('ngôn ngữ nói' => 'ngôn ngư noi')
    :param word:
    :return:
    """
    for i in range(60):
        if ls_vowel_tone[i] in word:
            # print(ls_vowel_tone[i])
            word = word.replace(ls_vowel_tone[i], ls_vowel_not_tone[i])
            # print(word)
    return word

def clean_special_character(word):
    for i in ls_special_character:
        if i in word:
            word = word.replace(i, '')
    return word

def clean_accent(word):
    """
    Convert untone words to unaccent words
    :param word:
    :return:
    """
    for i in range(12):
        if ls_vowel1[i] in word:
            # print(ls_vowel_tone[i])
            word = word.replace(ls_vowel1[i], ls_vowel1_not_accent[i])
            # print(word)
    word = word.replace('đ', 'd')
    return word

def cut_word_component(word):
    # require: word must have vowel and do not have tone
    """
    cut a word into 3 component: front_conso, vowel, back_conso
    :param word:
    :return:
    """
    if 'gi' == word:
        return {'front_conso': 'gi', 'vowel': 'i', 'back_conso': ''}
    ls_temp = []
    if 'gi' in word:
        word = word.replace('gi', 'g')
        ls_temp.append('gi')
    for i in ls_vowel:
        if i in word:
            ls_temp.extend(word.split(i))
            # print(ls_temp)
            return {'front_conso': ls_temp[0], 'vowel': i, 'back_conso': ls_temp[-1]}
    # print('At cut_word_component: do not have vowel in word')
    return {'front_conso': None, 'vowel': None, 'back_conso': None}


def check_valid_word(word):
    # require: do not have space, only one word
    """
    Check a word is exist in vietnamese or not
    :param word:
    :return:
    """
    word = clean_tone(word)
    # print(word)
    word = clean_special_character(word)
    countv = 0
    for v in ls_vowel1:
        if v in word:
            countv = countv + 1
    if countv==0:
        # print('Do not have vowel.')
        return False
    component = cut_word_component(word)
    if component['back_conso'] not in ls_conso and component['back_conso'] != '':
        # print('Do not exist the back-consonant.')
        return False
    if component['back_conso'] not in ls_conso_at_end and component['back_conso'] != '':
        # print('The consonant cannot stand at the end.')
        return False
    if component['front_conso'] not in ls_conso and component['front_conso'] != '':
        # print('Do not exist the front-consonant.')
        return False
    return True

if __name__ == '__main__':
    word = 'bỏ'
    if 'ờ' in word:
        print('tinh tinh')
    print(check_valid_word(word))