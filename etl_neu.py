from model.models import *
import os
import glob
import errno
from time import sleep

dir = os.getcwd()

path = dir+'/input/anhdaden2/*.txt'
files = glob.glob(path)
for name in files:
    try:
        with open(name) as f:
            file_name = os.path.basename(name)
            # continue
            line = f.readline()
            while line:
                print(line)
                neu = NeuSrc(value=line, file_name=file_name)
                db.session.add(neu)
                db.session.commit()

                line = f.readline()
            f.close()

            # pass
    except IOError as exc:
        if exc.errno != errno.EISDIR:
            raise

